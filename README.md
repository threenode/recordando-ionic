# Recordando Ionic App
----------

## Desarrollo
Desde consola, teniendo instalado nodejs con npm ejecutas:

- Dependencias Globales:
```
$ npm install -g bower gulp ionic cordova
```

- Instalar dependencias del proyectos:
```
$ npm install 
$ bower install 
```

## Ionic Tasks

- Servidor LiveReload
> 
```
$ ionic serve
```

- Compilar app 
> 
```
$ ionic platform add android
$ ionic build 
$ ionic run
```