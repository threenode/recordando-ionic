
var server = "http://recordando.esy.es";
angular.module('recordando', ['ionic', 'recordando.controllers', 'satellizer', 'permission', 'permission.ui'])

.constant("SERVER", {
  "base_url":       server,
  "api_url":        server+"/api/",
  "auth_url":       server+"/api/auth",
  "register_url":   server+"/api/register",
  "materia_url":    server+"/api/materia",
  "evaluacion_url": server+"/api/evaluacion",
  "port":           "80"
})

.run(function($ionicPlatform, RoleStore, $auth) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });

  //  PERMISOS DE USUARIO LOGEADO
  RoleStore
  .defineManyRoles({
    'LOGEADO': function(){ 
      if ($auth.isAuthenticated())
        return true
      else
        return false
    },
    'ANONIMO': function(){ 
      if (!$auth.isAuthenticated())
        return true
      else
        return false
    }
  });
  //  PERMISOS DE USUARIO LOGEADO

})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $authProvider, SERVER) {
  $ionicConfigProvider.tabs.position('bottom'); // TABS ABAJO
  // URL DE LOGIN SERVIDOR
  $authProvider.loginUrl = SERVER.auth_url;
  // URL DE LOGIN SERVIDOR

  $stateProvider
  .state('inicio', {
    url: '/inicio',
    templateUrl: 'templates/inicio.html',
    controller: 'InicioCtrl',
    controllerAs: 'inicio',
    data: {
      permissions: {
        except: ['LOGEADO'],
        redirectTo: 'tab.materias'
      }
    },
    cache: false,
  })

  .state('registro', {
    url: '/registro',
    templateUrl: 'templates/registro.html',
    controller: 'RegistroCtrl',
    controllerAs: 'registro',
    data: {
      permissions: {
        except: ['LOGEADO'],
        redirectTo: 'tab.materias'
      }
    },
    cache: false,
  })

  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  .state('tab.materias', {
    url: '/materias',
    views: {
      'tab-materias': {
        templateUrl: 'templates/tab-materias.html',
        controller: 'MateriasCtrl'
      }
    },
    data: {
      permissions: {
        except: ['ANONIMO'],
        redirectTo: 'inicio'
      }
    },
  })

  .state('tab.agregar-materia', {
    url: '/materias/agregar',
    views: {
      'tab-materias': {
        templateUrl: 'templates/agregar-materia.html',
        controller: 'agregarMateriaCtrl'
      }
    },
    data: {
      permissions: {
        except: ['ANONIMO'],
        redirectTo: 'inicio'
      }
    },
    cache: false,
  })

  .state('tab.evaluaciones', {
    url: '/evaluaciones/:materia/',
    views: {
      'tab-materias': {
        templateUrl: 'templates/evaluaciones.html',
        controller: 'EvaluacionesCtrl'
      }
    },
    data: {
      permissions: {
        except: ['ANONIMO'],
        redirectTo: 'inicio'
      }
    },
  })

  .state('tab.agregar-evaluacion', {
    url: '/evaluaciones/:materia/agregar',
    views: {
      'tab-materias': {
        templateUrl: 'templates/agregar-evaluacion.html',
        controller: 'agregarEvaluacionCtrl'
      }
    },
    data: {
      permissions: {
        except: ['ANONIMO'],
        redirectTo: 'inicio'
      }
    },
    cache: false,
  })

  .state('tab.usuario', {
    url: '/usuario',
    views: {
      'tab-usuario': {
        templateUrl: 'templates/tab-usuario.html',
        controller: 'UsuarioCtrl'
      }
    },
    data: {
      permissions: {
        except: ['ANONIMO'],
        redirectTo: 'inicio'
      }
    },
    cache: false,
  });

  // $urlRouterProvider.otherwise('/inicio');
  $urlRouterProvider.otherwise(function ($injector) {
    var $state = $injector.get('$state');
    $state.go('inicio');
  });

});
