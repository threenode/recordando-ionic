angular.module('recordando.controllers', [])


.filter('fecha', function() {
  return function(dateString) {
    return moment(new Date(dateString)).format("DD-MM-YYYY, h:ma")
  }
})
.filter('dentroDe', function() {
  return function(dateString) {
    return moment(new Date(dateString)).fromNow()
  }
})


//  CONTROLADOR INICIO SESION
.controller('InicioCtrl', function($scope, $auth, $http, $location, $rootScope, $ionicHistory, $state, SERVER) {
  $scope.loginData      = { };

  var self = this;
  $scope.hasError = false;
  $scope.login = function(credentials){
    console.log(credentials);
    $auth.login(credentials).then(function() {
      $scope.hasError = false;
      $http.get(SERVER.auth_url)
        .then(function (response) {
          var user = JSON.stringify( response.data.user );
          localStorage.setItem('user', response.data.user);
          $rootScope.currentUser = response.data.user;           
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go('tab.materias');
        }, function (error){
          console.log(response);
        });
    }).catch(function(error) {
      console.log(error);
      $scope.hasError = true;
      $scope.error    = error;
    });
  }
})
//  CONTROLADOR INICIO SESION


//  REGISTRO NUEVO USUARIO
.controller('RegistroCtrl', function($scope, $http, $state, $ionicHistory, SERVER) {
    $scope.userData = {};
    $ionicHistory.nextViewOptions({
      disableBack: true
    });

  var self = this;
  $scope.hasError = false;
  $scope.registrar = function(data){
    $http.post(SERVER.register_url, data)
    .then( function (response) {
      $state.go('inicio');
      $scope.hasError = false;
    }, function (error) {
      console.log(error);
      $scope.hasError = true;
      $scope.error = error;
    });
  };

  $scope.back = function(){
    $state.go('inicio');
  }
})
//  REGISTRO NUEVO USUARIO


// LIST MATERIAS
.controller('MateriasCtrl', function($scope, $http, $rootScope, SERVER) {
  $scope.materias     = [];
  $rootScope.materias = [];

  $scope.refreshMaterias = function(){
    $http({
      url: SERVER.materia_url,
      method: "GET"
    }).success(function(response, status, headers, config) {
      $scope.materias     = response.data;
      $rootScope.materias = response.data;
    }).finally(function() {
       $scope.$broadcast('scroll.refreshComplete');
     });
  };

  $scope.deleteMateria = function(index, materiaId){
    console.log(index, materiaId);
    $http({
      url: SERVER.materia_url + "/" + materiaId,
      method: "DELETE"
    }).success(function(response, status, headers, config) {
      $scope.refreshMaterias();
    });
  }

  $scope.$on("$ionicView.enter", function(event, data){
     $scope.refreshMaterias();
  });
})
// LIST MATERIAS


// AGREGAR NUEVA METERIA
.controller('agregarMateriaCtrl', function($scope, $http, $state, $ionicHistory, SERVER) {
  $scope.materiaData      = { };
  $ionicHistory.nextViewOptions({
    disableBack: true
  });

  $scope.addMateria = function(){
    var newMateria = {
      nombre:   $scope.materiaData.nombre,
      profesor: $scope.materiaData.profesor,
      seccion:  $scope.materiaData.seccion
    }
    console.log(newMateria);
      $http.post(SERVER.materia_url, newMateria)
      .then( success, error);
      function success(response) {
        console.log(response);
        $state.go('tab.materias');
      };
      function error(response){
        console.log(response);
      };
  }
  $scope.back= function(){
    $state.go('tab.materias');
  }
})
// AGREGAR NUEVA METERIA


//  LIST EVALUACIONES
.controller('EvaluacionesCtrl', function($scope, $stateParams, $http, $filter, $rootScope, SERVER) {
  var loadMaterias = function(){
    if (typeof $rootScope.materias == 'undefined') {
      $http({
        url: SERVER.materia_url,
        method: "GET"
      }).success( function(response, status, headers, config) {
        $rootScope.materias = response.data;
        $scope.materiaActual = $rootScope.materias[$stateParams.materia];
        $scope.refreshEvaluaciones();
      });
    }else{
      $scope.materiaActual = $rootScope.materias[$stateParams.materia];
      $scope.refreshEvaluaciones();
    }
  }

  $scope.evaluaciones = [];

  $scope.refreshEvaluaciones = function(){
    $http({
      url: SERVER.evaluacion_url,
      method: "GET",
      params: {materia_id: $scope.materiaActual.id}
    }).success(function(response, status, headers, config) {
      $scope.evaluaciones = response.data;
    }).finally(function() {
     $scope.$broadcast('scroll.refreshComplete');
   });
  };

    $scope.deleteEvaluacion = function(index, evaluacionId){
      // console.log(index, evaluacionId);
      $http({
        url: SERVER.evaluacion_url +"/"+ evaluacionId,
        method: "DELETE"
      }).success(function(response, status, headers, config) {
        $scope.refreshEvaluaciones();
      });
    }

    $scope.doneEvaluacion = function(index, evaluacionId){
      console.log(index, evaluacionId);
      $http({
        url: SERVER.evaluacion_url +"/"+ evaluacionId+"/done",
        method: "GET"
      }).success(function(response, status, headers, config) {
        $scope.refreshEvaluaciones();
      });
    }

    $scope.$on("$ionicView.enter", function(event, data){
     loadMaterias()
   });
  })
//  LIST EVALUACIONES


// AGREGAR NUEVA EVALUACION
.controller('agregarEvaluacionCtrl', function($scope, $http, $stateParams, $state, $ionicHistory, $filter, SERVER) {
  $scope.evaluacionData = {};

  $scope.addEvaluacion = function(){
    var newEvaluacion = {
      tipo:        $scope.evaluacionData.tipo,
      ponderacion: $scope.evaluacionData.ponderacion,
      fecha_hora:  $filter('date')($scope.evaluacionData.fecha, "yyyy-MM-dd") + ' ' + $filter('date')($scope.evaluacionData.hora, "HH:mm:ss"),
      materia_id:  $stateParams.materia
    }
    console.log(newEvaluacion);
      $http.post(SERVER.evaluacion_url, newEvaluacion)
      .then( success, error);
      function success(response) {
        console.log(response);
        $scope.back();
      };
      function error(response){
        console.log(response);
      };
  }
  $scope.back = function(){
    $ionicHistory.backView().go();
    // $state.go('tab.evaluaciones');
  }
})
// AGREGAR NUEVA EVALUACION




.controller('UsuarioCtrl', function($scope, $rootScope, $state, $auth) {

  $scope.logout = function(){
    localStorage.removeItem('user');
    $auth.logout();
    $rootScope.currentUser = null;
    $scope.user = null;
    $state.go('inicio');
  };
});
